// Copyright (c) 2019 ml5
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

/* ===
ml5 Example
Sound classification using SpeechCommands18w and p5.js
This example uses a callback pattern to create the classifier
=== */

// Initialize a sound classifier method with SpeechCommands18w model. A callback needs to be passed.
let classifier;
// Options for the SpeechCommands18w model, the default probabilityThreshold is 0
const options = { probabilityThreshold: 0.7 };
// Two variable to hold the label and confidence of the result
let label;
let confidence;


// Teachable Machine model URL:
let soundModel = 'http://localhost:8081/my_model/';



async function setup() {
    classifier = await ml5.soundClassifier(soundModel + 'model.json', options);
    // Create 'label' and 'confidence' div to hold results

    label = document.createElement('DIV');
    label.textContent = 'label ...';
    confidence = document.createElement('DIV');
    confidence.textContent = 'Confidence ...';

    //document.body.appendChild(label);
    //document.body.appendChild(confidence);
    // Classify the sound from microphone in real time
    classifier.classify(gotResult);
}
//setup();
const body = document.querySelector("body");

// A function to run when we get any errors and the results
function gotResult(error, results) {
    // Display error in the console
    if (error) {
        console.error(error);
    }
    // The results are in an array ordered by confidence.
    console.log(results);
    // Show the first label and confidence
    //label.textContent = 'Label: ' + results[0].label;
    switch(results[0].label){
        case "Red":
            body.style.backgroundColor = "red";
            break;
        case "Blue":
            body.style.backgroundColor = "blue";
            break;
        case "Green":
            body.style.backgroundColor = "green";
            break;
        default:
            body.style.backgroundColor = "white";
    }
   // confidence.textContent = 'Confidence: ' + results[0].confidence.toFixed(4);
}

/*
// Global variable to store the classifier
let classifier;

// Label
let label = 'listening...';

function preload() {
    // Load the model
    classifier = ml5.soundClassifier(soundModel + 'model.json');
}

function setup() {
    createCanvas(320, 240);
    // Start classifying
    // The sound model will continuously listen to the microphone
    classifier.classify(gotResult);
}

function draw() {
    background(0);
    // Draw the label in the canvas
    fill(255);
    textSize(32);
    textAlign(CENTER, CENTER);
    text(label, width / 2, height / 2);
}


// The model recognizing a sound will trigger this event
function gotResult(error, results) {
    if (error) {
        console.error(error);
        return;
    }
    // The results are in an array ordered by confidence.
    // console.log(results[0]);
    label = results[0].label;
}
*/
